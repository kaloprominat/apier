FROM python:2

ADD ./ /var/apier/

RUN pip install bottle
RUN pip install cherrypy

VOLUME /var/apier/modules

WORKDIR /var/apier/

CMD [ "python", "/var/apier/apier.py", "-c", "/var/apier/daemon.conf", "-f", "-l", "debug" ]

EXPOSE 8080