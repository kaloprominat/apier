#!/usr/bin/python
# -*- coding: utf-8 -*-

import bottle
import json
import datetime
import logging
import traceback
import threading

from xml.dom.minidom import Document


def local_property(name=None):
    if name: depr('local_property() is deprecated and will be removed.') #0.12
    ls = threading.local()
    def fget(self):
        try: return ls.var
        except AttributeError:
            raise RuntimeError("Request context not initialized.")
    def fset(self, value): ls.var = value
    def fdel(self): del ls.var
    return property(fget, fset, fdel, 'Thread-local property')


class apiermoduleException(Exception):
    """docstring for apiermoduleException"""
    def __init__(self, **kwargs):
        super(apiermoduleException, self).__init__()

        self._expose_details = True

        self.http_code = 500
        self.error_code = None
        self.message = None

        self._cause_exception = None
        self.cause_traceback =  None

        self._dict = {}

        for item in kwargs:
            if hasattr(self, item):
                self.__setattr__(item, kwargs[item])
            else:
                self._dict[item] = kwargs[item]

    def __str__(self):
        s = 'exception: %s\n' % self.__class__.__name__
        if self.expose_details:
            for attr, value in self.__dict__.iteritems():
                s += '%s: %s\n' % (attr, value)
        for attr, value in self._dict.iteritems():
            s += '%s: %s\n' % (attr, value)
        return s

    @property
    def cause_exception(self):
        return self._cause_exception

    @cause_exception.setter
    def cause_exception(self, new_cause):
        self._cause_exception = new_cause
        self.cause_traceback = traceback.format_exc(new_cause)
        if self._cause_exception and isinstance(self._cause_exception, apiermoduleException):
            self._cause_exception.expose_details = self._expose_details

    @property
    def expose_details(self):
        return self._expose_details

    @expose_details.setter
    def expose_details(self, new_var):
        self._expose_details = new_var
        if self._cause_exception and isinstance(self._cause_exception, apiermoduleException):
            self._cause_exception.expose_details = new_var

    def data_dict(self):
        return self._dict

    def dict(self):
        d = self._dict.copy()
        d.update(self.__dict__)

        rd = d.copy()

        for i in d:
            if hasattr(d[i], '__dict__'):
                rd[i] = d[i].__str__()

            if i.startswith('_'):
                del rd[i]

        return rd

    def unwrap_me(self):
        d = {}
        d['data'] = self.data_dict()
        d['code'] = self.error_code
        d['message'] = self.message
        if self._expose_details:
            d['exception'] = self.__class__.__name__
            if isinstance(self._cause_exception, apiermoduleException):
                d['cause'] = self._cause_exception.unwrap_me() #unwrap_self(self.cause_exception)
            elif self.cause_exception:
                d['cause'] = self._cause_exception.__class__.__name__
        return d

class apiermodule(object):

    """base class for constructing modules for apier"""

    __version__ = '1.0.1'


    apier_response_props = local_property()
    local_return_handler = local_property()


    # TODO: Deprecated, backward-comaptibility
    default_result = local_property()
    operational_result = local_property()

    def __init__(self, **kwargs):

        super(apiermodule, self).__init__()

        self.bottleapp = kwargs.get('bottleapp')
        self.apier_configs = kwargs.get('configs')

        self.loglevel = kwargs.get('loglevel', logging.DEBUG)
        self.log_dir = kwargs.get('log_dir', None)

        self.log_format = kwargs.get('log_format', None)

        if not hasattr(self, '_name'):
            self._name = 'apiermodule'

        self.init_logger()


        self.return_handler = self.JsonStatusReturner
        self.error_handler = self._Returner

    def destroy(self):
        self.logger.debug('obj: %s, releasing resources' % self)
        if hasattr(self, 'mlfh'):
            self.logger.removeHandler(self.mlfh)
            self.mlfh.close()

    def init_logger(self):

        if hasattr(self, 'loglevel'):

            self.logger = logging.getLogger(self.name)
            self.logger.setLevel(self.loglevel)

            if self.log_dir is not None:
                self.mlfh = logging.FileHandler('%s/%s.log' % (self.log_dir, self.name))
                self.mlfh.setLevel(self.loglevel)

                if self.log_format is not None:
                    mlf = logging.Formatter(self.log_format)
                    self.mlfh.setFormatter(mlf)

                self.logger.addHandler(self.mlfh)

    @property
    def log_dir(self):
        if hasattr(self, '_log_dir'):
            return self._log_dir

    @log_dir.setter
    def log_dir(self, value):
        self._log_dir = value

    @property
    def name(self):

        if hasattr(self, '_name'):
            return self._name
        else:
            return None

    @name.setter
    def name(self, new_name):

        if self.name == 'apiermodule' or self.name != new_name:

            self._name = new_name

            self.init_logger()

            # if self.log_dir is not None:
            #     self.mlfh = logging.FileHandler('%s/%s.log' % (self.log_dir, self.name))
            #     self.mlfh.setLevel(self.loglevel)

            #     if self.log_format is not None:
            #         mlf = logging.Formatter(self.log_format)
            #         self.mlfh.setFormatter(mlf)

            #     self.logger.addHandler(self.mlfh)

        

        # self.BindRoutes()
    def after_init(self):

        self.BindRoutes()

    # returners functions

    def _Return_res(self):
        response = {
            'body': '',
            'http_headers' : {},
            'http_code' : 200
        }
        return response

    def Error(self, **kwargs):


        if hasattr(self.error_handler, '__call__'):
            return self.error_handler(**kwargs)
        else:
            return self._Returner(**kwargs)

    def _LocalReturner(self, **kwargs):
        kwargs['response'] = kwargs.get('response', self._Return_res())
        return self.local_return_handler(**kwargs)

    def _Returner(self, **kwargs):

        kwargs['response'] = kwargs.get('response', self._Return_res())
        return self.return_handler(**kwargs)

    def BasicReturner(self, **kwargs):

        res = kwargs.get('response')

        data = kwargs.get('data', None)
        err = kwargs.get('error', None)

        if err is not None:

            if isinstance(err, apiermoduleException):
                res['http_code'] = err.http_code
            else:
                res['http_code'] = 500

            res['body'] = err

        else:
            res['body'] = data

        res['body'] = res['body'].__str__()

        return res

    def JsonReturner(self, **kwargs):


        res = kwargs.get('response')

        data = kwargs.get('data', None)
        err = kwargs.get('error', None)

        res['http_headers'].update({'Content-Type': 'application/json'})

        if err is not None:

            res['body'] = {'error' : {}}

            if isinstance(err, apiermoduleException):

                res['http_code'] = err.http_code
                res['body']['error'] = err.unwrap_me()

                if err.expose_details:
                    res['body']['error']['traceback'] = traceback.format_exc()

            else:

                res['body']['error'] = traceback.format_exc(err)
                res['http_code'] = 500

        else:

            #   TODO: Deprecated, some backward-compatibility
            if self.operational_result is not None:
                res['body'] = self.operational_result
            else:
                res['body'] = data
            

        res['body'] = json.dumps(res['body'])
        return res

    def JsonStatusReturner(self, **kwargs):

        res = kwargs.get('response')

        data = kwargs.get('data', None)
        err = kwargs.get('error', None)

        res['http_headers'].update({'Content-Type': 'application/json'})
        res['body'] = {
            'status'    : 0,
            'code'      : 0,
            'message'   : None,
            'data'      : data
        }

        if err is not None:
            res['body']['status'] = 1

            if isinstance(err, apiermoduleException):
                res['body']['code'] = err.error_code
                res['body']['data'] = err.data_dict()
                res['body']['message'] = err.message
                # if err.expose_details:
                res['body']['error'] = err.unwrap_me()
            else:
                res['body']['data'] = err.__str__()

        res['body'] = json.dumps(res['body'])

        return res


    def XmlReturner(self, **kwargs):

        res = kwargs.get('response')

        data = kwargs.get('data', None)
        err = kwargs.get('error', None)

        res['http_headers'].update({'Content-Type': 'application/xml'})

        def _xml_from_data(data):

            doc = Document()

            def build(father, structure):
                if type(structure) == dict:

                    for k in structure:
                        tag = doc.createElement(str(k))
                        father.appendChild(tag)
                        build(tag, structure[k])

                elif type(structure) == list:
                    grandFather = father.parentNode
                    tagName = str(father.tagName)
                    grandFather.removeChild(father)
                    for l in structure:
                        tag = doc.createElement(tagName)
                        build(tag, l)
                        grandFather.appendChild(tag)

                else:
                    data = str(structure)
                    tag = doc.createTextNode(data)
                    father.appendChild(tag)

            if len(data) == 1:
                rootName = str(data.keys()[0])
                root = doc.createElement(rootName)

                doc.appendChild(root)
                build(root, data[rootName])

            return doc.toprettyxml(indent="  ")

        if err is None:
            res['body'] = _xml_from_data({'data': data})
        else:

            res['body'] = {'error' : {}}

            if not isinstance(err, apiermoduleException):
                res['http_code'] = 500
                res['body']['error'] = err

            else:
                res['http_code'] = err.http_code
                res['body']['error'] = err.unwrap_me()

                if err.expose_details:
                    res['body']['error']['traceback'] = traceback.format_exc()


                # res['body'] = _xml_from_data({'error': err.__dict__})
                # res['http_code'] = err.http_code if isinstance(err, apiermoduleException) else 500

        res['body'] = _xml_from_data(res['body'])

        return res

    def RawReturner(self, **kwargs):

        res = kwargs.get('response')

        data = kwargs.get('data', None)
        err = kwargs.get('error', None)

        if err is None:
            res['body'] = data
        else:
            res['body'] = err

        return res


    def __str__(self):
        return '<%s (apiermodule %s) at %s>' % (self.name, apiermodule.__version__, hex(id(self)))

    def __repr__(self):
        return self.__str__()

    def WriteLog(self, logstring, loglevel='info', thread=None):

        if hasattr(self, 'logger'):
            self.logger.__getattribute__(loglevel)(logstring)

    def ProccessRoute(self, **kwargs):

        #   TODO: deprecated, backward-compatibiliy

        self.default_result = { 'status' : 0, 'code' : 0, 'data': None, 'message': None }
        self.operational_result = self.default_result.copy()

        response = self._Return_res()

        #   TODO: legacy, should be deprecated in feature

        AnswerOptions = {
            'ResponseType' : 'default',
            'ResponseData' : {},
            'ResponseHeader' : {}
        }
        #   TODO: legacy

        self.apier_response_props = { 'ResponseType' : 'default' }
        self.local_return_handler = self.return_handler

        try:
            Request = self.ParseRequest(kwargs)
        except apiermoduleException as e:
            ret_data = self.Error(error=e, response=response)
        except Exception as e:
            ret_data = self.Error(error = apiermoduleException(
                                    cause_exception = e,
                                    message = "Unhandled error '{0}' while parsing request".format(e)),
                                response=response)
        else:

            try:
                data = self.routes[Request['matched_route']]['function'](Request, response=response, options=AnswerOptions)
            except apiermoduleException as e:
                ret_data = self.Error(error = e, response = response)
            except Exception as e:
                ret_data = self.Error(
                            error = apiermoduleException(
                                cause_exception = e,
                                message = "Unhandled error '{0}' in module function {1}".format(e, self.routes[Request['matched_route']]['function'])),
                            response=response)

            else:

                #   TODO: legacy, should be deprecated in feature
                #   need to remove any checks and leave only _Returner() call

                if AnswerOptions['ResponseType'] != 'default':
                    self.logger.warn('DEPRECATED AnswerOptions use detected at {0}: {1}, use self.setResponse[Property] methods'.format(self.routes[Request['matched_route']]['function'], AnswerOptions))
                    self.setResponseType(AnswerOptions['ResponseType'])

                if AnswerOptions['ResponseHeader'] != {}:
                    self.setResponseHeader(AnswerOptions['ResponseHeader'])

                if AnswerOptions['ResponseData'] != {}:
                    self.ModifyResponseData(AnswerOptions['ResponseData'])

                # if AnswerOptions == AnswerOptions_bak:
                #     ret_data = self._Returner(data=data, response=response)
                # else:

                #     self.logger.warn('DEPRECATED AnswerOptions use detected at {0}: {1}'.format(self.routes[Request['matched_route']]['function'], AnswerOptions))

                #     if AnswerOptions['ResponseType'] == 'default':
                #         ret_data = self._Returner(data=data, response=response)
                #     elif AnswerOptions['ResponseType'] == 'plain':
                #         ret_data = self.BasicReturner(data=data, response=response)
                #     elif AnswerOptions['ResponseType'] == 'json':
                #         ret_data = self.JsonReturner(data=data, response=response)
                #     elif AnswerOptions['ResponseType'] == 'xml':
                #         ret_data = self.XmlReturner(data=data, response=response)
                #     elif AnswerOptions['ResponseType'] == 'raw':
                #         ret_data = self.RawReturner(data=data, response=response)
                #     else:
                #         ret_data = self._Returner(data=data, response=response)
                
                ret_data = self._LocalReturner(data = data, response = response)

        # applying response attributes

        # if AnswerOptions['ResponseType'] != 'raw':

        if self.local_return_handler != self.RawReturner:

            for header in ret_data['http_headers']:
                bottle.response.set_header(header, ret_data['http_headers'][header])

            bottle.response.status = ret_data['http_code']


        if 'ResponseHeaders' in self.apier_response_props:
            for header in self.apier_response_props['ResponseHeaders']:
                hval = self.apier_response_props['ResponseHeaders'][header]
                if header == 'Content-Type':
                    bottle.response.content_type = hval
                bottle.response.set_header(header, hval)

        if 'ResponseStatusCode' in self.apier_response_props:
            bottle.response.status = self.apier_response_props['ResponseStatusCode']

        # TODO: deprecated, need to be removed

        # if hasattr(bottle.response,'apier_params'):
        #     self.logger.warn('DEPRECATED ModifyResponseHeader use detected at {0}: {1}'.format(self.routes[Request['matched_route']]['function'], bottle.response.apier_params))
        #     if 'status' in bottle.response.apier_params:
        #         bottle.response.status = bottle.response.apier_params['status']

        #     if 'headers' in bottle.response.apier_params:
        #         for header in bottle.response.apier_params['headers']:
        #             if header[0] == 'Content-Type':
        #                 bottle.response.content_type = header[1]
        #             bottle.response.set_header(header[0], header[1])


        self.logger.debug('Response: %s' % ret_data)

        return ret_data['body']

    # def ProccessRoute2(self, **kwargs):
        

    #     Request = self.ParseRequest(kwargs)
    #     Response = None

    #     self.operational_result = self.default_result.copy()

    #     AnswerOptions = {
    #         'ResponseType' : 'default',
    #         'ResponseData' : {},
    #         'ResponseHeader' : {}
    #     }

    #     try:
    #         Response = self.routes[Request['matched_route']]['function'](Request, options=AnswerOptions)
    #         result = self.operational_result
    #     except Exception as e:
    #         result = self.operational_result
    #         result['status'] = 1
    #         result['code'] = 1
    #         result['data'] = None
    #         result['message'] = "Unhandled error '%s' in module function %s" % (e, self.routes[Request['matched_route']]['function'])
    #         self.WriteLog("%s %s: Unhandled error '%s' in module function %s" % ( Request['bottle.request'].method,Request['matched_route'] , e, self.routes[Request['matched_route']]['function']), 'error', self.name)
    #         self.WriteLog('%s' % traceback.format_exc(e), 'error', self.name )
    #     else:
    #         result['data'] = Response

    #     try:
    #         # json.dumps(Response)
    #         self.returner_function(Response)
    #     except Exception as e:
    #         result['status'] = 1
    #         result['code'] = 2
    #         result['data'] = None
    #         result['message'] = 'Module function %s returned unserializable data: \'%s\'' % (self.routes[Request['matched_route']]['function'], e)

    #         self.WriteLog("%s %s: Module function %s returned unserializable data: '%s'" % ( Request['bottle.request'].method, Request['matched_route'] , self.routes[Request['matched_route']]['function'],e), 'error', self.name)
    #         self.WriteLog('%s' % traceback.format_exc(e), 'error', self.name )

    #     if AnswerOptions['ResponseType'] == 'default':
    #         return result
    #     else:
    #         return result['data']

    def BindRoutes(self):

        added_routes = {}

        for route in self.routes:
            if not hasattr(self.routes[route]['function'], '__call__'):
                self.WriteLog('Function variable for route %s is not actually callable: %s' % (route, self.routes[route]['function']), 'warn', self.name)
            if hasattr(self, 'base_url'):
                f_route = '%s/%s' % (self.base_url, route)
                f_route = f_route.replace('//','/')
                added_routes.update({f_route: self.routes[route]})
            else:
                f_route = route
            self.bottleapp.route(f_route, method=self.routes[route]['method'])(self.ProccessRoute)
            self.WriteLog('Binded route %s with method %s to function %s' % (f_route, self.routes[route]['method'], self.routes[route]['function']), 'debug', self.name)

        self.routes.update(added_routes)

    def ParseRequest(self, kwargs):

        data = {
                "bottle.request": bottle.request,
                "bottle.response": bottle.response,
                "http_headers":[],
                "http_headers_dict":{},
                "get": [],
                "get_dict": {},
                "post": [],
                "post_dict": {},
                "path": None,
                "matched_route" : None,
                "matched_route_method" : None,
                "variables": kwargs,
                "post_body": None,
                "body" : None
                }
        data['path'] = bottle.request.path

        data['matched_route'] = bottle.request['route.handle'].rule
        data['matched_route_method'] = bottle.request['route.handle'].method

        data['body'] = bottle.request.body.read()
        data['post_body'] = data['body']

        content_type = ''

        for header in bottle.request.headers:
            if header == 'Content-Type':
                content_type = bottle.request.headers[header]
            data['http_headers'].append({header: bottle.request.headers[header]})
            data['http_headers_dict'].update({header: bottle.request.headers[header]})

        for getitem in bottle.request.query:
            data['get'].append({getitem: bottle.request.query.get(getitem)})
            data['get_dict'].update({getitem: bottle.request.query.get(getitem)})

        if 'json' in content_type:
            try:
                data['post'] = json.loads(data['body'])
            except ValueError as e:
                raise apiermoduleException(exception = '{0}: bad json post request'.format(apiermoduleException.__name__),
                                            http_code = 400,
                                            message = 'application/json content passed, but parse error occured')
        
        elif 'form' in content_type:
            for postitem in bottle.request.POST:
                data['post'].append({postitem:bottle.request.POST.get(postitem)})
                data['post_dict'].update({postitem: bottle.request.POST.get(postitem)})
        else:
            data['post'] = data['body']

        return data

    #   TODO: deprecated, need to be removed
    def ModifyResponseHeader(self, parametrs):

        self.logger.warn('DEPRECATED ModifyResponseHeader use detected: {0}'.format(parametrs))

        if 'status' in parametrs:
            self.setResponseStatusCode(parametrs['status'])

        if 'headers' in parametrs:
            for header, value in parametrs['headers']:
                self.setResponseHeader({header: value})

    #   TODO: deprecated, need to be removed

    def ModifyResponseData(self, parametrs):

        self.logger.warn('DEPRECATED ModifyResponseData use detected: {0}'.format(parametrs))

        self.setResponseType('json')

        response_data = self.operational_result
        for item in parametrs:
            response_data[item] = parametrs[item]

    def setResponseHeader(self, header):
        if 'ResponseHeaders' not in self.apier_response_props:
            self.apier_response_props['ResponseHeaders'] = {}
        self.apier_response_props['ResponseHeaders'].update(header)

    def setResponseStatusCode(self, code):
        self.apier_response_props['ResponseStatusCode'] = code

    def setResponseType(self, response_type):
        # self.apier_response_props['ResponseType'] = response_type

        if response_type == 'default':
            self.local_return_handler = self.return_handler
        elif response_type == 'plain':
            self.local_return_handler = self.BasicReturner
        elif response_type == 'json':
            self.local_return_handler = self.JsonReturner
        elif response_type == 'jsonstatus':
            self.local_return_handler = self.JsonStatusReturner
        elif response_type == 'xml':
            self.local_return_handler = self.XmlReturner
        elif response_type == 'raw':
            self.local_return_handler = self.RawReturner
        else:
            self.local_return_handler = self.return_handler
