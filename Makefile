all: sdist
    
sdist:
	python setup.py sdist

install-apiermodule:
	@sudo python setup.py install

clean:
	rm -rf ./venv

virtualenv:
	virtualenv --version || ( echo "No virtualenv installed!"; exit 127 )

venv: virtualenv
	if [ ! -d ./venv ]; then \
	virtualenv --no-site-packages ./venv ;\
	source venv/bin/activate ;\
	pip install bottle==0.10.6 ;\
	pip install cherrypy ;\
	fi

run: venv
	source venv/bin/activate ;\
	python apier.py -c ./daemon.conf -f -l debug ;\
	deactivate


docker-run-rebuild: docker-stop docker-build-image docker-run

docker-build-image:
	docker build -t docker_apier .

docker-logs:
	set -x ;\
	cid="$$(docker ps -a | grep "docker_apier" | awk '{ print $$1 }')" ;\
	docker logs -f $$cid

docker-run:
	docker ps | grep "docker_apier" || docker run -d -p 8080:8080/tcp -v "$$(pwd)"/modules:/var/apier/modules docker_apier
	make -e docker-logs

docker-stop:
	set -x ;\
	cid="$$(docker ps -a | grep "docker_apier" | awk '{ print $$1 }')" ;\
	docker stop $$cid ;\
	docker rm $$cid ;\